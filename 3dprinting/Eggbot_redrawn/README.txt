                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:844167
Eggbot redrawn by jsc is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

I wanted to make a few modifications to nglasson's eggbot for my own build. As a first step, I've redrawn it in Fusion 360 because I don't like working in Sketchup (sorry Sketchup fans), and Fusion's STL output is of higher fidelity. This model is mostly faithful to the original, except I've modified a few screw sizes to fix interference. I've also added the steppers, servo, and all hardware to the model assembly to help in ordering parts.  

**Update**: v2 has a few updates and fixes:  

* The pen arm shaft coupler axle hole has been resized to fit the axle on my stepper motor (5mm around, flat face inset .5mm, .1mm outside clearance).  
* The outer bearing holder has been integrated into the right end plate. The idler must now be on the right, unless you mirror this, but now you can replace four long screws with one medium one. This should be printed flat side down with support for the bearing hole.  
* The inner bearing hole in the end plate has been shrunk from 24mm to 22mm to fit properly.  
* The slot for the brace attachment has been changed to a hole, and the right brace modified to match up.  

**Update**: v3 now contains a pen linkage spring. I attached one end to the servo horn with a twist of wire.  Also added an Arduino holder with standoffs for screws (M3x8 or #4 1/4").  

BOM now under Instructions.  

Video: https://www.youtube.com/watch?v=WwdGjqfIo0U

# Instructions

Hardware required:

7 M3x10 screws for all but one screw of the motor mounts.
2 M3x14 screws for top screw hole on brace arms.
1 M3x20 screw for pen arm linkage spring
2 M3x8 screws as set screws for stepper motors shafts
3 more M3x8 if you want to mount an Arduino UNO to the rods (there are four standoffs, but one is unusable because of clearance issues)
3 M3 nuts
10 M3 washers
2 M4x10 screws to attach pen arm pivot to pen arm shaft coupler
1 M4x35 screw as pen arm hinge
1 M4x16 as pen set screw
2 M4 washers
4 M4 nuts
22 M8 nuts to position everything on the rods; two go on both ends of the idler rod
3 [M8x300 threaded rods](http://www.mcmaster.com/#99055a315)
1 [M8x100](http://www.mcmaster.com/#99055a313) threaded rod for the idler
1 [spring for the idler](http://www.mcmaster.com/#9657k33), ID slightly larger than 8mm
2 [608ZZ bearings](http://www.amazon.com/gp/product/B002BBGTK6)
2 [NEMA 17 stepper motors](https://www.adafruit.com/products/324)
1 [9g micro servo](https://www.adafruit.com/products/169)
2 [silicone o-rings](http://www.amazon.com/gp/product/B000FMUV02), 1/2" ID 3/4" OD 1/8" width

All screws, nuts, and washers can be bought in odd lots at boltdepot.com. I recommend adafruit.com for the motors and servo, and Amazon for the bearings. Threaded rod can be bought from mcmaster.com.

For the electronics, I used an Arduino Uno with the Adafruit Motor Shield. I had to make some major edits to the SphereBot firmware to get everything to work correctly. Modified firmware [here](https://github.com/jinschoi/SphereBot), you will also need the Adafruit motor shield library.

I had occasion recently to write up some explicit instructions on how to get going with my exact setup, here it is:

Installing firmware:

Get the Arduino IDE from https://www.arduino.cc/en/Main/Software.
Get the firmware from https://github.com/jinschoi/SphereBot (download ZIP button)
Open up SphereBot.ino with the IDE.
a. Under Tools, ensure Board is set to “Arduino/Genuino Uno” and Port is set to the proper serial port.
b. Under Sketch, Include Library, Manage Libraries…, search for adafruit motor shield v2, and make sure it is installed.
Plug the USB cable to your hardware and upload the firmware using the right arrow button.
Verify that it is working:

Click the serial monitor button in the upper right of the IDE.
Make sure you have “Newline” and "115200 baud” selected in the lower right drop downs.
X MOTOR

Type “G0 X1600” into the top field. The egg motor should spin 180 degrees with the side facing you traveling down (counter clockwise looking at the face of the motor).
Type “G0 X0”, it should rotate back to the starting position.
Y MOTOR

Manually center the pen arm.
Type “G1 Y480”. The pen arm should travel counter clockwise (to your left) to its limit. Make sure it’s not hitting anything.
Type “G1 Y-480”, the pen arm should now swing all the way to the right. Again, make sure it isn’t hitting anything.
If your motors are not moving in these directions, it is okay as long as they are BOTH moving in the “wrong” direction. Otherwise, everything will come out backwards. If they are moving as described above, UP is to the left, towards the motor. If only one axis is not moving as above, then you need to flip the wires for that axis.
PEN SERVO

Recenter the arm with “G1 Y0”, then enter “M300 S100”. That will lift the pen to the top of its default travel.
Enter “M300 S115”, that should gently lower the pen a little bit.
“M300 S100” should pop the pen back up quickly.
Mount an egg and pen, and lower it slowly using M300 commands until the pen is close to but not touching the egg. When the egg rotates, it can vary a little bit, so you don’t want to be too close, but you want to minimize the gap. This should be your default pen up position. Then slowly lower the arm until it contacts the egg, and add a little extra to apply some pressure. That will be your pen down position.
Set your pen up position using M303 Pxxx, M500.
The pen is clamped to values from 100 to 130 by default. If you need to extend these, you can use “M301 Pxx” to decrease the pen up value and “M302 Pxxx” to increase the pen down value. M500 to save the results for the future.
Inkscape

Download Inkscape: https://inkscape.org/en/download/
Get my version of the inkscape-unicorn plugin: https://github.com/jinschoi/inkscape-unicorn
Install it by copying the CONTENTS of src/ to the inkscape extensions folder (so the extensions directory should contain unicorn.inx, unicorn.py, and the unicorn directory).
Run Inkscape, start a new document. Under File, Document Properties, Page, set a custom size of 3200 width, 800 height in units px (or load up a downloaded SVG for the eggbot that has that size).
Under File, Save As, select MakerBot Unicorn G-Code, click Save.
In the following dialog, double check all the settings:
Setup: Pen up and pen down angles should be correct for you. Delay after pen up and pen down I have set to 100. XY feedrate 300, Z-axis is irrelevant.
Pen Registration Check: turn off
Homing: 0, 0
Copies: 1
Pen Changes: turn on pause on layer changes
Send the gcode file using Utils/gcode-sender.rb from my firmware project.
Gcode Sender
If you are on Linux or Mac OS X:

sudo gem install serialport (you may need to install some developer tools to get this to work)
chmod 755 /path-to-gcode-sender/gcode-sender.rb
/path-to-gcode-sender/gcode-sender.rb my-file.gcode
If you are on Windows:

Install the latest ruby using Rubyinstaller: http://rubyinstaller.org/downloads/ (make sure you use the right one: x64 for 64-bit, not-x64 for 32-bit).
Down below on that page, also get the correct DevKit-mingw for the Ruby you installed and 32 or 64-bit. Unpack it into its own directory somewhere.
Open a command window, go to the directory you unpacked devkit, run devkitvars.bat.
In that same window, run “gem install serialport”
Edit the gcode-sender.rb file with a text editor to change the port variable at the top to COM3 (or whatever).
Go to the directory with your gcode file. Run “ruby \path\to\gcode-sender.rb myfile.gcode”
You only have to do steps 2-4 once.

It will pause when it hits M0, which the Inkscape gcode plugin should insert before every layer to let you switch pens. Hit return to continue.