                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1683764
Sphere-O-Bot (eggbot MOD) by jjrobots is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

The Sphere-O-bot is an open source hardware+software project. Originally from Evil Mad Scientist (very cool robots), we have adapted the idea to our electronics and ancillary elements. Software, designs and use is exactly the same. 
The frame was designed by Attila Nagy (Thingiverse nick: dibloff)

The list of parts to print:
1x Spring holder
1x MAIN_frame (with support)
1x Egg support
1x Pen holder
2x Axis support
1x vertical ARM
1x Rod spacer
 
 The Electronics are OPEN SOURCE (feel free to replicate it if you want)
<h5>You can also buy everything (customizable KIT):<a href="http://jjrobots.com/product/sphere-o-bot/"> here </a></h5> 
<h5>or create all the jjRobots with 
<a href="http://jjrobots.com/product/awesomeroboticskit/">the MOST AWESOME ROBOTICS KIT</a></h5>
<h4>How to set it up?</h4>
Take a look to this link: http://jjrobots.com/sphere-o-bot-assembly-and-user-guide/

https://www.youtube.com/watch?v=UrGMX7NNNxc


# Print Settings

Printer Brand: Ultimaker
Printer: Ultimaker 2
Rafts: No
Supports: Yes
Resolution: 0.20
Infill: 30

Notes: 
PLA works nicely.

# Post-Printing

![Alt text](https://cdn.thingiverse.com/assets/51/e0/18/b6/b8/all.jpg)

# How I Designed This

<h3>What you need to create this painting robot</h3>
2x 623 bearing
1x Threaded steel rod (3mmØ,80mm length)
1x compression spring (4,5mmØ, 10 mm length)
2x <a href="http://www.jjrobots.com/product/stepper-motor-nema17/">1.8deg HIGH QUALITY NEMA 17 Stepper motors (40mm length) (4.4Kg/cm torque)
Motor cables (14+70 cms long)</a>
1x SG90 servo
1x <a href="http://www.jjrobots.com/product/arduino-leonardo-clone-usb-cable/">Arduino Leonardo compatible</a>
1x <a href="https://jjrobots.com/product/b-robot-electronic-brain-shield-v2-0/">jjRobots Brain Shield</a>
2x A4988 Stepper motor drivers
1x Power supply 12v/2A
6mm M3 bolts
12mm M3 bolts
M3 nuts
2x 20mm Suction Cups
1x M3 wing nut

<h3>OTHERS:</h3>
cable wrap (optional but makes everything look prettier..)

You can also buy the Sphere-O-Bot robot KIT (motors, electronics and ancillary elements) here:
http://jjrobots.com/product/sphere-o-bot/


# Custom Section

![Alt text](https://cdn.thingiverse.com/assets/9d/2e/71/1b/11/Untitled3.gif)

## Iboardbot, B-robot EVO...all the JJrobots

If you have created a B-robot EVO or the iBoardbot, you already have the electronics and almost all the ancillary elements to create your Sphere-O-bott! (yes, we have used the same items to create this drawing bot). 
Take a look to the <a href="http://www.thingiverse.com/thing:1069256"> B-robot EVO,</a><a href="http://www.thingiverse.com/thing:1590702"> the iBoardbot </a>and...<a href="http://jjrobots.com/product/awesomeroboticskit/">THE MOST AWESOME ROBOTICS KIT</a>

![Alt text](https://cdn.thingiverse.com/assets/89/b4/e4/33/1f/open_source_3.png)